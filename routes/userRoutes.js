const express=require('express')
const router=express.Router();
const userControllers=require('../controllers/userControllers')
const auth=require('../auth')

router.post('/checkEmail',userControllers.checkEmailExists)
router.post('/register',userControllers.checkEmailExists,userControllers.registerUser)
router.post('/login',userControllers.loginUser)
router.post('/details',auth.verify,userControllers.userDetails)
router.get('/profile',userControllers.profileDetails)

module.exports=router;