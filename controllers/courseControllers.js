const Courses=require('../models/Courses');
const User=require('../models/User')

module.exports.addCourse=(req,res)=>{
    let newCourse=new Courses({
        name:req.body.name,
        description: req.body.description,
        price: req.body.price,
        slots: req.body.slots
    })
    let admin = req.body.isAdmin
    if(!admin){
        newCourse.save()
        .then(result=>{
            console.log(result)
            res.send(true)
        })
        .catch(err=>{
            console.log(err)
            res.send(false)
        })
    }else{
        res.send('Hindi ka admin beh, wag assuming')
    }
}

/* 
newCourse.save()
    .then(result=>{
        console.log(result)
        res.send(true)
    })
    .catch(err=>{
        console.log(err)
        res.send(false)
    })
*/