const { rmSync } = require('fs');
const User=require('../models/User');
const bcrypt=require('bcrypt');
const auth=require('../auth');
const { nextTick } = require('process');

// *Check if email already exists
/* 
*steps:
*1.use mongoose 'find' method to find duplicate emails
*2.use .then method
*/
module.exports.checkEmailExists=(req,res,next)=>{
    return User.find({email:req.body.email})
    .then(result=>{
        // console.log(req.body.email)
        let message=``;
        if(result.length>0){
            message=`The ${req.body.email} is already taken, please use other email.`
            return res.send(message);
        }else{
            next();
        }
    })
}

module.exports.registerUser=(req,res)=>{
    // *creates variable "newUSer" and instantiates a new 'User' object using mongoose model
    // *uses the information from the req.body to provide the necessary info
    let newUser = new User({
        firstName:req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNo: req.body.mobileNo
    })
    // *saves the created object to our database
    return newUser.save().then(user=>{
        console.log(user);
        res.send(`congrats sir/maam ${newUser.firstName}! You are now registered`)
    })
    .catch(err=>{
        console.log(err);
        res.send(`Sorry ${newUser.firstName}, there was an error during the registration. Please try again!`)
    })
}

module.exports.loginUser=(req,res)=>{
    // *the findOne method, returns the first record in the collection that matches the search criteria.
    return User.findOne({email: req.body.email})
    .then(result=>{
        if(result===null){
            res.send(`Your email: ${req.body.email} is not registered yet. Register first!`)
        // *the compareSync method is used to compare a non encrypted pw from the login from the encrypted passwrd retrieve. it will return true or false value depending on the result
        }else{
            const isPasswordCorrect=bcrypt.compareSync(req.body.password, result.password)
            if(isPasswordCorrect){
                return res.send({accessToken: auth.createToken(result)})
            }else{
                return res.send('password incorrect')
            }}
    })
}

module.exports.userDetails=(req,res)=>{
    const id=req.body.id;
    return User.findById(id)
    .then(result=>{
        result.password=""
        return res.send(result)
    })
    .catch(err=>{
        return res.send(err)
    })
}

module.exports.profileDetails=(req,res)=>{
    // *user will be object that contains the id and email of the user that currently logged in
    const userData=auth.decode(req.headers.authorization);

    return User.findById(userData.id)
    .then(result=>{
        result.password="******"
        return res.send(result)
    }).catch(err=>{
        return res.send(err)
    })
}